<?php

function _civicrm_api3_apimt940_parse_spec(&$params) {
  $params['realpath']['api.required'] = 1;
  $params['realpath']['api.default'] = '';
}

/**
 * Update donor's statuses.
 *
 * @param $params
 *
 * @return array
 */
function civicrm_api3_apimt940_parse($params) {
  try {
    $parserParams = new CRM_Apimt940_Parser_Params($params);
    $statementObject = new CRM_Apimt940_Parser_Statement($parserParams);
    $statements = $statementObject->get($statementObject->content);
    $transactionObject = new CRM_Apimt940_Parser_Transaction($statements);
    $transactions = $transactionObject->get();
    return civicrm_api3_create_success($transactions, $params);
  } catch (Exception $exception) {
    $data = array(
      'params' => $params,
      'exception' => array(
        'message' => $exception->getMessage(),
        'file' => $exception->getFile(),
        'line' => $exception->getLine(),
        'object' => $exception,
      ),
    );
    return civicrm_api3_create_error('Problem parsing MT940 file', $data);
  }
}

<?php

class CRM_Apimt940_Parser_Params {

  public $realpath = '';

  public $offset = 1;

  public $limit = 0;

  private $keys = array(
    'realpath',
    'offset',
    'limit',
  );

  function __construct($params) {
    foreach ($this->keys as $key) {
      if (array_key_exists($key, $params)) {
        $this->$key = $params[$key];
      }
    }
  }
}

<?php

class CRM_Apimt940_Parser_Fields {

  private $statement;

  public function __construct($statement) {
    $this->statement = $statement;
  }

  public function getTransactionalReferenceNumber() {
    return $this->statement[CRM_Apimt940_Dict_Label::TRANSACTION_REFERENCE_NUMBER];
  }

  public function getFirstName() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::INFORMATION_TO_LINE];
    $re = '/\/BO\/([a-zA-Z\-]*)/';
    if (preg_match($re, $str, $matches)) {
      return CRM_Apimt940_Helper::mbUcwords($matches[1]);
    }
    return '';
  }

  public function getLastName() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::INFORMATION_TO_LINE];
    $re = '/\/BO\/[a-zA-Z\-]*\ ([a-zA-Z\-]*)/';
    if (preg_match($re, $str, $matches)) {
      return CRM_Apimt940_Helper::mbUcwords($matches[1]);
    }
    return '';
  }

  // todo verify label OB3, should be BO3!
  public function getStreet() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::INFORMATION_TO_LINE];
    $re = '/\/OB3\/([a-zA-Z0-9\ \.]*)\//';
    if (preg_match($re, $str, $matches)) {
      return CRM_Apimt940_Helper::mbUcwords($matches[1]);
    }
    return '';
  }

  // todo verify label OB4, should be BO4!
  public function getPostalCode() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::INFORMATION_TO_LINE];
    $re = '/\/OB4\/([0-9]{2}-[0-9]{3})/';
    if (preg_match($re, $str, $matches)) {
      return $matches[1];
    }
    return '';
  }

  // todo verify label OB4, should be BO4!
  public function getCity() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::INFORMATION_TO_LINE];
    $re = '/\/OB4\/[0-9]{2}-[0-9]{3}\ ([a-zA-Z0-9\ \-\.]*)\//';
    if (preg_match($re, $str, $matches)) {
      return CRM_Apimt940_Helper::mbUcwords($matches[1]);
    }
    return '';
  }

  // todo where is this field located?
  public function getCountryId() {
    return 1172;
  }

  public function getAccountNumber() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::INFORMATION_TO_LINE];
    $re = '/\/BO\/[a-zA-Z0-9\ \.]*([A-Z]{2}[0-9]*)/';
    if (preg_match($re, $str, $matches)) {
      return $matches[1];
    }
    return '';
  }

  public function getTransactionType() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::STATEMENT_LINE];
    $re = '/[0-9]{10}[A-Z]{2,3}[0-9]*,[0-9]{2}N([A-Z]{3})/';
    if (preg_match($re, $str, $matches)) {
      return $matches[1];
    }
    return '';
  }

  public function getAmount() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::STATEMENT_LINE];
    $re = '/[0-9]{10}[A-Z]{2,3}([0-9]*,[0-9]{2})/';
    preg_match($re, $str, $matches);
    return (float)str_replace(',', '.', $matches[1]);
  }

  public function getCurrency() {
    $str = $this->statement[CRM_Apimt940_Dict_Label::OPENING_BALANCE];
    $re = '/[A-Z][0-9]{6}([A-Z]{2,3})/';
    preg_match($re, $str, $matches);
    return $matches[1];
  }

  public function getReceiveDate() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::STATEMENT_LINE];
    $date = substr($str, 0, 6);
    return DateTime::createFromFormat('ymd', $date)->format('Y-m-d');
  }

  public function getPaymentDetails() {
    $str = @$this->statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1][CRM_Apimt940_Dict_Label::INFORMATION_TO_LINE];
    $re = '/\/PY\/([a-zA-Z0-9\ ]*)\//';
    if (preg_match($re, $str, $matches)) {
      return CRM_Apimt940_Helper::mbUcwords($matches[1]);
    }
    return '';
  }
}

<?php

class CRM_Apimt940_Parser_Statement {

  private $params;

  public $content = '';

  public $sheet = array();

  function __construct(CRM_Apimt940_Parser_Params $params) {
    $this->params = $params;
    $this->load();
  }


  /**
   * Load file to content variable.
   */
  private function load() {
    $sheet = file($this->params->realpath);
    $this->content = implode('', $sheet);
  }


  /**
   * Get statements from content of file.
   *
   * @param string $content
   *
   * @return array
   */
  public function get($content) {
    $sts = explode(CRM_Apimt940_Dict_Label::TRANSACTION_REFERENCE_NUMBER, $content);
    unset($sts[0]); // first element is empty
    $sts = $this->limitRows($sts);
    $statements = array();
    foreach ($sts as $st) {
      if ($statementId = $this->getStatementId($st)) {
        $tab = $this->parseStatementToArray($statementId, $st);
        $tab = $this->convertStatementToTags($tab);
        $tab = $this->cleanTags($tab);
        $statements[$statementId] = $tab;
      }
    }
    return $statements;
  }


  private function getStatementId($statementContent) {
    $re = '/^([0-9a-zA-Z])+/';
    preg_match_all($re, $statementContent, $matches, PREG_SET_ORDER, 0);
    return $matches[0][0];
  }


  private function parseStatementToArray($statementId, $statementContent) {
    $statementContent = substr(trim($statementContent), 0, -1);
    $statementContent = str_replace($statementId, CRM_Apimt940_Dict_Label::TRANSACTION_REFERENCE_NUMBER . $statementId, $statementContent);
    $statementContent = trim($statementContent);
    $re = '/(:[0-9A-Z]{2,3}:)/';
    $split = preg_split($re, $statementContent, -1, PREG_SPLIT_NO_EMPTY + PREG_SPLIT_DELIM_CAPTURE);
    return array_map('trim', $split);
  }


  private function convertStatementToTags($statementArray) {
    $tags = array();
    $statementLineIndex = 0;
    for ($i = 0; $i < count($statementArray); $i = $i + 2) {
      $tagId = $statementArray[$i];
      if ($tagId == CRM_Apimt940_Dict_Label::STATEMENT_LINE) {
        $tags[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][++$statementLineIndex][$tagId] = $statementArray[$i + 1];
      } elseif ($tagId == CRM_Apimt940_Dict_Label::INFORMATION_TO_LINE) {
        $tags[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][$statementLineIndex][$tagId] = $statementArray[$i + 1];
      } else {
        $tags[$tagId] = $statementArray[$i + 1];
      }
    }
    return $tags;
  }


  private function cleanTags($statementTags) {
    foreach ($statementTags as $k => $tag) {
      if (is_array($tag)) {
        $statementTags[$k] = $this->cleanTags($tag);
      } else {
        $statementTags[$k] = $this->cleanTag($tag);
      }
    }
    return $statementTags;
  }


  private function cleanTag($tag) {
    $tag = str_replace("\r\n", '', $tag);
    $tag = preg_replace('/\s+/', ' ',$tag);
    return $tag;
  }


  /**
   * Index of arrays are preserved and the same as in source file.
   *
   * @param array $wholeSheet
   *
   * @return array
   */
  private function limitRows($wholeSheet) {
    if ($this->params->limit > 0 || $this->params->offset > 1) {
      $sheet = array_slice($wholeSheet, $this->params->offset - 1, $this->params->limit, TRUE);
    } else {
      $sheet = $wholeSheet;
    }
    return $sheet;
  }
}

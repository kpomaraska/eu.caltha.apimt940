<?php

class CRM_Apimt940_Parser_Transaction {

  private $statements;

  public function __construct($statements) {
    $this->statements = $statements;
  }

  /**
   * Get transactions based on statement format.
   *
   * @return array
   */
  public function get() {
    $transactions = array();
    foreach ($this->statements as $trn => $sts) {
      $lines = $this->explode($sts);
      $trxs = $this->extract($lines);
      $transactions = array_merge($transactions, $trxs);
    }
    return $transactions;
  }

  /**
   * Explode statement into separate lines.
   * Sometimes statement includes more lines :61: than 1.
   *
   * @param array $statement
   *
   * @return array
   */
  private function explode($statement) {
    $n = count($statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY]);
    $tab = array();
    for ($i = 1; $i <= $n; $i++) {
      $item = $statement;
      unset($item[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY]);
      $item[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][1] = $statement[CRM_Apimt940_Dict_Label::STATEMENT_GROUP_KEY][$i];
      array_push($tab, $item);
    }
    return $tab;
  }

  /**
   * Extract lines to desired format.
   *
   * @param array $lines
   *
   * @return array
   */
  private function extract($lines) {
    $transactions = array();
    foreach ($lines as $ln) {
      $item = $this->formatRow($ln);
      array_push($transactions, $item);
    }
    return $transactions;
  }


  /**
   * Format one line in desired format.
   *
   * @param array $row
   *
   * @return array
   */
  private function formatRow($row) {
    $fields = new CRM_Apimt940_Parser_Fields($row);
    return array(
      'reference' => $fields->getTransactionalReferenceNumber(),
      'transaction_type' => $fields->getTransactionType(),
      'receive_date' => $fields->getReceiveDate(),
      'account_number' => $fields->getAccountNumber(),
      'total_amount' => $fields->getAmount(),
      'currency' => $fields->getCurrency(),
      'details' => $fields->getPaymentDetails(),
      'first_name' => $fields->getFirstName(),
      'last_name' => $fields->getLastName(),
      'street_address' => $fields->getStreet(),
      'postal_code' => $fields->getPostalCode(),
      'city' => $fields->getCity(),
      'country_id' => $fields->getCountryId(),
    );
  }
}

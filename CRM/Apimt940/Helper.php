<?php

class CRM_Apimt940_Helper {

  /**
   * Format first letter in multibyte string
   *
   * @param $string
   *
   * @return string
   */
  private static function mbUcfirst($string) {
    $firstChar = mb_strtoupper(mb_substr($string, 0, 1));
    return $firstChar . mb_substr($string, 1);
  }


  /**
   * Format each word in sentence (separate by dash, space or dot)
   *
   * @param $string
   *
   * @return string
   */
  public static function mbUcwords($string) {
    $delimiters = array('-', ' ', '.');
    $string = mb_strtolower($string);
    $missing_delimiters = 0;
    foreach ($delimiters as $delimiter) {
      if (strpos($string, $delimiter) !== false) {
        $tab = explode($delimiter, $string);
        foreach ($tab as $k => $v) {
          if (mb_strlen($v) > 1) {
            $tab[$k] = self::mbUcfirst($v);
          }
        }
        $string = implode($delimiter, $tab);
      } else {
        $missing_delimiters++;
      }
    }
    if ($missing_delimiters == count($delimiters)) {
      $string = self::mbUcfirst($string);
    }
    return $string;
  }
}

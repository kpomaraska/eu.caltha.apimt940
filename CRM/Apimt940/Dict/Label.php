<?php

class CRM_Apimt940_Dict_Label {
  const TRANSACTION_REFERENCE_NUMBER = ':20:';
  const ACCOUNT_IDENTIFICATION = ':25:';
  const STATEMENT_NUMBER = ':28:';
  const OPENING_BALANCE = ':60F:';
  const STATEMENT_LINE = ':61:';
  const INFORMATION_TO_LINE = ':86:';
  const CLOSING_BALANCE = ':62F:';
  const CLOSING_AVAILABLE_BALANCE = ':64:';

  const STATEMENT_GROUP_KEY = ':6186:';

  public static $ids = array(
    self::TRANSACTION_REFERENCE_NUMBER,
    self::ACCOUNT_IDENTIFICATION,
    self::STATEMENT_NUMBER,
    self::OPENING_BALANCE,
    self::STATEMENT_LINE,
    self::INFORMATION_TO_LINE,
    self::CLOSING_BALANCE,
    self::CLOSING_AVAILABLE_BALANCE,
  );
}
